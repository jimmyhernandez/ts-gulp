var gulp = require("gulp");
var browserify = require("browserify");
var source = require("vinyl-source-stream");
//var watchify = require("watchify");
var tsify = require("tsify");
//var uglify = require("gulp-uglify");
var sourcemaps = require("gulp-sourcemaps");
var buffer = require("vinyl-buffer");
//var gutil = require("gulp-util");
//var ts = require("gulp-typescript");
//var tsProject = ts.createProject("tsconfig.json");
var paths = {
    pages: ['src/*.html']
}

/*var watchedBrowserify = watchify(
    browserify({
        basedir: ".",
        debug: true,
        entries: ["src/main.ts"],
        cache: {},
        packageCache: {}
    })
    .plugin(tsify)
);*/

gulp.task("copy-html",function() {
    return gulp.src(paths.pages)
    .pipe(gulp.dest("dist"));
});


gulp.task("default",function(){
    return browserify({
        basedir: ".",
        debug: true,
        entries: ["src/main.ts"],
        cache: {},
        packageCache: {}
    })
    .plugin(tsify)
    .transform("babelify")
    .bundle()
    .pipe(source("bundle.js"))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("dist"));
});


/*function bundle(){
    return watchedBrowserify
    .bundle()
    .pipe(source("bundle.js"))
    .pipe(gulp.dest("dist"));
}

gulp.task("default",["copy-html"],bundle);
watchedBrowserify.on("update",bundle);
watchedBrowserify.on("log",gutil.log);*/